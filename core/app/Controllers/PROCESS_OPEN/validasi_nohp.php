<?php

$arr_cek = $db->cekNohpMemberReg($nohp_email);
//$password = $jreq->password;
if (is_numeric($nohp_email) == TRUE) {
    $jenis_msg = 'NOHP';
} else {
    $jenis_msg = 'EMAIL';
}
if (isset($arr_cek->id)) {
    if ($arr_cek->tipe != 'M3') {
        $response = array(
            'response_code' => '0123',
            'response_message' => $jenis_msg. ' Anda tidak dapat digunakan untuk transaksi Mobile karena sudah terdaftar.'
        );
        die(json_encode($response));
    }
    //sudah terdaftar
    $key_validasi = $fungsi->randomNumber(4);

    $message = "$nama_aplikasi, KODE VALIDASI = $key_validasi . Rahasiakan kode validasi untuk keamanan transaksi. Kode dapat digunakan dalam 5 menit.";
    $sql = "BEGIN TRANSACTION;"
            . "insert into log_validasi (noid,djson_validasi,kode_validasi) values"
            . "('$nohp_email','$param','$key_validasi');"
            . "COMMIT;";

    $db->singleRow($sql);
    $db->kirimMessageUnreg($nohp_email, $message);

    $response = array(
        'response_code' => '0000',
        'response_message' => 'Kode Validasi ' . $key_validasi . ' sedang dikirim ke ' . $jenis_msg . ' ' . $nohp_email . ' KODE DAPAT DIGUNAKAN DALAM 5 MENIT'
    );
} else {
    //belum terdaftar harus isi kode agen
    $response = array(
        'response_code' => '0123',
        'response_message' => $jenis_msg . ' anda belum terdaftar, Masukkan Kode Agen untuk pendaftaran.'
    );
}


$reply = json_encode($response);
