<?php

namespace Controllers;

use Resources,
    Models,
    Libraries;

class SERVICE extends Resources\Controller {

    public function __construct() {
        parent::__construct();
        $this->rest = new Resources\Rest;
    }

    public function coba() {
        $cobi = array(
            'oke' => 'oke'
        );

        $arr_coba = json_decode(json_encode($cobi));
        $arr_coba->okeh = 'aneh';
        var_dump($arr_coba);
    }

    public function setting_product($token, $noid) {
        $db = new Models\Databases();
        if ($token == 'piewuroijlkjdio3892iolkejwoi938iuewi') {

            $sql_member = "select fee from tbl_member_account where noid='$noid'";
            $result_member = $db->singleRow($sql_member);
            $arr_fee = json_decode($result_member->fee);

            $sql = "select product,product_detail,default_margin from tbl_product_map "
                    . "order by product,product_detail;";
            $result = $db->multipleRow($sql);
            $setting_fee = array();
            if (isset($result[0]->product)) {
                foreach ($result as $fetch) {
                    if (isset($arr_fee->{$fetch->product . $fetch->product_detail})) {
                        $setting_fee[$fetch->product . $fetch->product_detail] = $arr_fee->{$fetch->product . $fetch->product_detail};
                    } else {
                        $setting_fee[$fetch->product . $fetch->product_detail] = $fetch->default_margin;
                    }
                }
                echo json_encode($setting_fee);
            } else {
                echo '{}';
            }
        } else {
            echo 'invalid token';
        }
    }

    public function list_message($tipe, $token) {
        $db = new Models\Databases();
        if ($token == 'piewuroijlkjdio3892iolkejwoi938iuewi') {
            $nohp_email = 'nohp_email';
            if ($tipe == 'SMS') {
                $nohp_email = 'nohp_email as nohp';
            }
            $sql = "select id,waktu,$nohp_email,msg from log_message "
                    . "where stat = 0 and interface='$tipe' order by id;";
            $result = $db->multipleRow($sql);

            if (isset($result[0]->id)) {
                foreach ($result as $fetch) {
                    $sql_update = "update log_message set stat=1 where id = $fetch->id";
                    $db->singleRow($sql_update);
                }
                echo json_encode($result);
            } else {
                echo '[]';
            }
        } else {
            echo 'invalid token';
        }
    }

    public function service_add_va_bni($token) {
        $db = new Models\Databases();
        $dbVa = new Models\DbVa();
        $konfig = new Libraries\Konfigurasi;

        if ($token == 'piewuroijlkjdio3892iolkejwoi938iuewi') {

            $sql = "select id, noid, nama, nohp_email, jva from tbl_member_account "
                    . "where jenis = 1 and jva::jsonb->>'BNI' = '0' order by id;";
            $result = $db->multipleRow($sql);
            $url = "http://dsyariah.co.id/tki/va/create_open_va.php";
            $email = "";
            if (isset($result[0]->id)) {
                foreach ($result as $fetch) {
                    if (is_numeric($fetch->nohp_email) !== TRUE) {
                        $email = $fetch->nohp_email;
                        $sql_insert = "insert into tbl_nomor_va_bnis (noid,bank,keterangan) values ('$fetch->noid','BNI','insert') returning id";
                        $arr_insert = $dbVa->singleRow($sql_insert);
                        $id_va = str_pad(($arr_insert->id + 100000), 12, "0", STR_PAD_LEFT);
                    } else {
                        $email = '';
                        $sql_insert = "insert into tbl_nomor_va_bnis (noid,bank,keterangan) values ('$fetch->noid','BNI','insert') returning id";
                        $arr_insert = $dbVa->singleRow($sql_insert);
                        $id_va = $fetch->nohp_email;
                    }

                    echo $request = '{"noid": "' . $fetch->noid . '",'
                    . '"nohp": "' . $id_va . '",'
                    . '"nama": "' . $fetch->nama . '",'
                    . '"email": "' . $email . '",'
                    . '"nominal":0}';
                    //tembak service va
//                    $contents = $this->rest->sendRequest($url, 'POST', $request);
                    $contents = '{"status": "0000","virtual_account": "VA' . $id_va . '"}';

                    $jrespon = json_decode($contents);
                    if ($jrespon->status == '0000') {
                        $sql_update = "update tbl_nomor_va_bnis set nomor_va='$jrespon->virtual_account',keterangan='$fetch->nama' where id=$arr_insert->id;";
                        $jva = json_decode($fetch->jva);
                        $jva->BNI = $jrespon->virtual_account;
                        $string_jva = json_encode($jva);
                        $sql_update_mb = "update tbl_member_account set jva = '$string_jva' where id = $fetch->id;";
                        $db->singleRow($sql_update_mb);
                        $msg_out = $konfig->namaAplikasi() . ", NOMOR VA BNI (009), Untuk TOPUP SALDO " . $jrespon->virtual_account . " a.n " . $fetch->nama;
                        $db->kirimMessage($fetch->noid, $msg_out);
                    } else {
                        $sql_update = "delete from tbl_nomor_va_bnis where id=$arr_insert->id;";
                    }
                    $dbVa->singleRow($sql_update);
                    break;
                }
            } else {
                echo 'tidak ada data';
            }
        } else {
            echo 'invalid token';
        }
    }

    public function refresh_today_trx($token) {
        $db = new Models\Databases();
        if ($token == 'piewuroijlkjdio3892iolkejwoi938iuewi') {
            $sql = "update tbl_member_account set today_trx = 0;"
                    . "update tbl_member_channel set today_trx = 0";
            $result = $db->singleRow($sql);
            echo 'ok';
        } else {
            echo 'invalid token';
        }
    }

    public function posting_data($token) {
        $db = new Models\Databases();
        if ($token == 'piewuroijlkjdio3892iolkejwoi938iuewi') {
            $sql = "update tbl_member_account set today_trx = 0;"
                    . "update tbl_member_channel set today_trx = 0";
            $result = $db->singleRow($sql);
            echo 'ok';
        } else {
            echo 'invalid token';
        }
    }

    public function backup_channel_trx($token) {
        $db = new Models\Databases();
        if ($token == 'piewuroijlkjdio3892iolkejwoi938iuewi') {
            $sql = "update tbl_member_account set today_trx = 0;"
                    . "update tbl_member_channel set today_trx = 0";
            $result = $db->singleRow($sql);
            echo 'ok';
        } else {
            echo 'invalid token';
        }
    }

    public function proses_tiket_deposit($token) {
        $db = new Models\Databases();
        $fungsi = new Libraries\Fungsi();
        $konfig = new Libraries\Konfigurasi;
        if ($token == '893U489HOUEW9823U8W73U8W89381JN') {
            $sql = "select id,waktu,bank,ket,nominal,dc,waktu_bank,saldo,executed,noid "
                    . "from bank_capture where executed=0 and date(waktu) = date(now()) order by id;";
            $result = $db->multipleRow($sql);
            if (isset($result[0]->id)) {
                foreach ($result as $fetch) {

                    $nominal = $fetch->nominal;
                    $bank = $fetch->bank;
                    $keterangan = $fetch->ket;
                    $action = 'exec'; //inq exec
                    $bukti_add = $keterangan;
                    $sql_cek_konf = "select * from log_konfirmasi_topup where nominal = $nominal and bank = '$bank' "
                            . "and status = 0 and date(waktu) = date(now()) order by id desc limit 1;";
                    $arr_cek_konf = $db->singleRow($sql_cek_konf);

                    if (isset($arr_cek_konf->id)) {
                        $id_add = $arr_cek_konf->id;
                        $noid_add = $arr_cek_konf->noid;
                        $amount_add = $arr_cek_konf->nominal;
                        $reff_add = $arr_cek_konf->reff;
                        $tujuan_add = $arr_cek_konf->bank;
                        $keterangan_add = $arr_cek_konf->keterangan;

                        $arr_cek_mbr = $db->cekNoidMember($noid_add);

                        if (!isset($arr_cek_mbr->id)) {
                            $error->accountTidakAda(0);
                        }
                        $noid_add = $arr_cek_mbr->noid;
                        $nama_add = $arr_cek_mbr->nama;
                        $tipe_add = $arr_cek_mbr->tipe;
                        $nohp_email_add = $arr_cek_mbr->nohp_email;

                        $interface = 'ROBOT';
                        $tipe_member = 'ADMIN';
                        $noid = '0000000000010001';
                        $nama_member = 'TIKET_DEPOSIT_' . $bank;
                        $username = $nama_member;
                        $ref = date("ymdH") . $fungsi->randomNumber(8);

                        if ($noid_add == $noid) {
                            $error->accountTidakValid(0);
                        }

                        if ($amount_add < 1000) {
                            $error->minimalTransaksi(0);
                        }

                        $amount_minus = $amount_add * -1;
                        $data_trx = array(
                            'username' => $username,
                            'interface' => $interface,
                            'product' => 'TIKET DEPOSIT',
                            'product_detail' => 'SALDO',
                            'idpel' => $noid_add,
                            'idpel_name' => $nama_add,
                            'amount' => $amount_minus,
                            'keterangan' => $keterangan_add,
                            'bank' => $tujuan_add,
                            'reff' => $ref,
                            'trace_id' => $ref,
                            'lembar' => 1,
                            'response_code' => '0000',
                            'response_message' => 'sukses'
                        );
                        $jlast_trx = json_encode($data_trx);
                        if ($tipe_member == 'ADMIN') {
                            $saldo_last = 0; //karena ADMIN saldo unlimit
                            //sql_insert manual
                            $sql_insert_manual = "insert into log_data_trx (waktu,noid,reff,amount,saldo,detail) "
                                    . "values (now(),'$noid','$ref','$amount_minus','$saldo_last','$jlast_trx');";
                            $db->singleRow($sql_insert_manual);
                        } else {
                            $sql_kurang_saldo = "update tbl_member_account set saldo = saldo + $amount_minus,last_trx='$jlast_trx',last_amount='$amount_minus',last_reff='$ref',last_fee='{}',last_lembar=1 where noid = '$noid' returning saldo;";
                            $saldo_noid = $db->singleRow($sql_kurang_saldo);
                            $saldo_last = $saldo_noid->saldo;
                        }
                        $data_trx_add = array(
                            'username' => $nama_member,
                            'interface' => $interface,
                            'product' => 'TIKET DEPOSIT',
                            'product_detail' => 'SALDO',
                            'idpel' => $noid,
                            'idpel_name' => $nama_member,
                            'amount' => $amount_add,
                            'keterangan' => $keterangan_add,
                            'bank' => $tujuan_add,
                            'reff' => $ref,
                            'trace_id' => $ref,
                            'lembar' => 1,
                            'response_code' => '0000',
                            'response_message' => 'sukses'
                        );
                        $jlast_trx_noid = json_encode($data_trx_add);
                        $sql_tambah_saldo = "update tbl_member_account set saldo = saldo + $amount_add,last_trx='$jlast_trx_noid',last_amount=$amount_add,last_reff='$ref',last_fee='{}',last_lembar=1 where noid = '$noid_add' returning saldo;";
                        $saldo_add = $db->singleRow($sql_tambah_saldo);
                        $saldo_add_last = $saldo_add->saldo;

                        $response = array(
                            'response_code' => '0000',
                            'saldo' => $saldo_last,
                            'response_message' => "Validasi TOPUP Tiket Deposit SALDO KE $noid_add a.n $nama_add Rp. " . $fungsi->rupiah($amount_add) . " BERHASIL. REFF : $ref",
                            'trace_id' => $ref
                        );

                        $msg_out = $konfig->namaAplikasi() . ", TOPUP Tiket Deposit Bank $bank SALDO Rp. " . $fungsi->rupiah($amount_add) . " BERHASIL REFF $ref. Saldo Rp. " . $fungsi->rupiah($saldo_add_last);
                        $db->kirimMessage($noid_add, $msg_out);



                        $sql_update_konf = "update log_konfirmasi_topup set status=1, waktu_proses = now(), "
                                . "noid_executor = '$noid', bukti_transfer = '$bukti_add' where id = $id_add;";
                        $db->singleRow($sql_update_konf);
                    } else {
                        $response = array(
                            'response_code' => '0403',
                            'response_message' => "TOPUP GAGAL, Data Konfirmasi Deposit tidak ditemukan"
                        );
                    }
                    
                    $sql_update = "update bank_capture set executed=1,noid='$noid_add' where id = $fetch->id";
                    $db->singleRow($sql_update);
                    sleep(1);
                }
                echo json_encode($result);
            }
            echo 'ok';
        } else {
            echo 'invalid token';
        }
    }

}
