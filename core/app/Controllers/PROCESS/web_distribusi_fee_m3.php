<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$bulan = strtoupper($jreq->detail->bulan);
$tahun = strtoupper($jreq->detail->tahun);
$action = $jreq->detail->action;
$filter_def_noid = '';
$amount_total = 0;

$sql = "select noid, sum(lembar) as jumlah, sum(cast(jfee->>'fm3' as integer)) as fee_m3, "
        . "sum(cast(detail->>'admin_bank' as integer)) as admin_bank from log_data_trx "
        . "where response_code='0000' and jfee->>'fm3' != '0' and posting = 0 "
        . "and detail::jsonb->>'product_detail' != 'SALDO' and stat = 1 and to_char(waktu,'YYYYMM')='$tahun$bulan' group by noid order by noid asc;";

$arr = $db->multipleRow($sql);

if (!isset($arr[0]->noid)) {
    $error->dataFeeTidakDitemukan($bulan,$tahun);
}

if ($action == 'exec') {

    foreach ($arr as $each) {
        $noid_member = $each->noid;
        $jumlah_trx = $each->jumlah;
        $total_fee = $each->fee_m3;

        $bank = 'FEE';
        $keterangan = 'Distribusi Fee ' . $jumlah_trx . ' trx';
        $bukti_add = $keterangan;


        $noid_add = $noid_member;
        $amount_add = $total_fee;
        $tujuan_add = $bank;
        $keterangan_add = $keterangan;

        $arr_cek_mbr = $db->cekNoidMember($noid_add);
        
        if(isset($arr_cek_mbr->noid)){
        $noid_add = $arr_cek_mbr->noid;
        $nama_add = $arr_cek_mbr->nama;
        $tipe_add = $arr_cek_mbr->tipe;
        $nohp_email_add = $arr_cek_mbr->nohp_email;

        $interface = 'MANUAL';
        $tipe_member = 'ADMIN';
        $noid = '0000000000010001';
        $nama_member = 'DISTRIBUSI_' . $bank;
        $username = $nama_member;
        $ref = date("ymdH") . $fungsi->randomNumber(8);

        if ($noid_add == $noid) {
            $error->accountTidakValid(0);
        }

        if ($amount_add < 10) {
            $error->minimalTransaksi(0);
        }

        $amount_minus = $amount_add * -1;
        $data_trx = array(
            'username' => $username,
            'interface' => $interface,
            'product' => 'DISTRIBUSI_FEE',
            'product_detail' => 'SALDO',
            'idpel' => $noid_add,
            'idpel_name' => $nama_add,
            'amount' => $amount_minus,
            'keterangan' => $keterangan_add,
            'bank' => $tujuan_add,
            'reff' => $ref,
            'trace_id' => $ref,
            'lembar' => 1,
            'response_code' => '0000',
            'response_message' => 'sukses'
        );
        $jlast_trx = json_encode($data_trx);
        if ($tipe_member == 'ADMIN') {
            $saldo_last = 0; //karena ADMIN saldo unlimit
            //sql_insert manual
            $sql_insert_manual = "insert into log_data_trx (waktu,noid,reff,amount,saldo,detail) "
                    . "values (now(),'$noid','$ref','$amount_minus','$saldo_last','$jlast_trx');";
            $db->singleRow($sql_insert_manual);
        } else {
            $sql_kurang_saldo = "update tbl_member_account set saldo = saldo + $amount_minus,last_trx='$jlast_trx',last_amount='$amount_minus',last_reff='$ref',last_fee='{}',last_lembar=1 where noid = '$noid' returning saldo;";
            $saldo_noid = $db->singleRow($sql_kurang_saldo);
            $saldo_last = $saldo_noid->saldo;
        }
        $data_trx_add = array(
            'username' => $nama_member,
            'interface' => $interface,
            'product' => 'DISTRIBUSI_FEE',
            'product_detail' => 'SALDO',
            'idpel' => $noid,
            'idpel_name' => $nama_member,
            'amount' => $amount_add,
            'keterangan' => $keterangan_add,
            'bank' => $tujuan_add,
            'reff' => $ref,
            'trace_id' => $ref,
            'lembar' => 1,
            'response_code' => '0000',
            'response_message' => 'sukses'
        );
        $jlast_trx_noid = json_encode($data_trx_add);
        $sql_tambah_saldo = "update tbl_member_account set saldo = saldo + $amount_add,last_trx='$jlast_trx_noid',last_amount=$amount_add,last_reff='$ref',last_fee='{}',last_lembar=1 where noid = '$noid_add' returning saldo;";
        $saldo_add = $db->singleRow($sql_tambah_saldo);
        $saldo_add_last = $saldo_add->saldo;
        $amount_total += $amount_add;

        $msg_out = $konfig->namaAplikasi() . ", DISTRIBUSI FEE anda $bulan $tahun Rp. " . $fungsi->rupiah($amount_add) . " BERHASIL REFF $ref. Saldo Rp. " . $fungsi->rupiah($saldo_add_last);
        $db->kirimMessage($noid_add, $msg_out);
        }
    }

    $response = array(
        'response_code' => '0000',
        'saldo' => $saldo_last,
        'response_message' => "Distribusi Fee M3 Berhasil. Total Fee M3 : " . $fungsi->rupiah($amount_total),
        'trace_id' => $ref
    );

    $reply = json_encode($response);

    $sql_update_posting = "update log_data_trx set posting=1 where to_char(waktu,'YYYYMM')='$tahun$bulan';";
    $db->singleRow($sql_update_posting);
} else {
    $response['response_code'] = '0000';
    $response['response_message'] = 'OK';
    $response['data'] = $arr;

    $reply = json_encode($response);
}