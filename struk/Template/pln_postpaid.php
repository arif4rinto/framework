<?php

$judul_struk = 'STRUK PEMBAYARAN TAGIHAN LISTRIK';
$jst = json_decode($obj->struk);
$penanda = $obj->cetak == 1 ? 'CA' : 'CU';
$waktu = $obj->waktu;
$noid = $obj->noid;

$idpel = $jst->idpel;
$nama = $jst->nama;
$segmen = $jst->segmen;
$daya = $jst->daya;
$lembar = $jst->lembar;
$ref = $jst->reff;
$info = $jst->info;
$vsn = $jst->vsn;
$pesan_lunas = 'PLN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH';
$lembarProses = $lembar > 4 ? 4 : $lembar;


$tagihan = 0;
$admin_bank = 0;
$totalBayarPlusAdmin = 0;
$tahun_tag = "";
$bulan_tag = "";
$nama_bulans = "";
$period = "";
$standmeter1 = "";
$standmeter2 = "";

$idpel = $idpel;
$nama = $nama;

for ($x = 0; $x < $lembarProses; $x++) {
    $tagihan = $tagihan + $jst->detail[$x]->tagihan;
    $admin_bank = $admin_bank + $jst->detail[$x]->admin_bank;
    $tahun_tag = substr($jst->detail[$x]->billPeriod, 2, 2);
    $bulan_tag = substr($jst->detail[$x]->billPeriod, 4, 2);
    $nama_bulans = get_bulan($bulan_tag);
    $period = $period . $nama_bulans . $tahun_tag . ", ";
}
$totalBayarPlusAdmin = $tagihan + $admin_bank;
$info = $info;
$standmeter1 = $jst->detail[0]->standmeter1;
$standmeter2 = $jst->detail[($x - 1)]->standmeter2;
$arrInfo = explode("\n", wordwrap("PLN menyatakan struk ini sebagai bukti pembayaran yang sah.", 35, "\n"));
$infoTunggakan = "TERIMA KASIH|$info";

$arrInfoTunggakan = explode("|", $infoTunggakan);

imagestring($im, 3, (imagesx($im) - 7 * strlen("STRUK PEMBAYARAN TAGIHAN LISTRIK")) / 2, 110, "STRUK PEMBAYARAN TAGIHAN LISTRIK", $black);
imagestring($im, 1, 8, 130, "IDPEL      :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 130, $idpel, $black);
imagestring($im, 1, 8, 140, "NAMA       :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 140, $nama, $black);
imagestring($im, 1, 8, 150, "TARIF/DAYA :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 150, $segmen . "/" . $daya . "VA", $black);
imagestring($im, 1, 8, 160, "BL/TH      :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 160, rtrim($period, ", "), $black);
imagestring($im, 1, 8, 170, "STAND METER:", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 170, $standmeter1 . '-' . $standmeter2, $black);
imagestring($im, 1, 8, 180, "RP TAG PLN :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 180, "RP " . rupiah($tagihan), $black);
imagestring($im, 1, 8, 190, "NO REF     :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 190, $ref, $black);

imagestring($im, 1, (imagesx($im) - 3 * strlen($arrInfo[0])) / 4, 205, $arrInfo[0], $black);
imagestring($im, 1, (imagesx($im) - 3 * strlen($arrInfo[1])) / 3, 215, $arrInfo[1], $black);

imagestring($im, 1, 8, 235, "ADMIN BANK :", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 235, "RP " . rupiah($admin_bank), $black);
imagestring($im, 1, 8, 245, "TOTAL BAYAR:", $black);
imagestring($im, 1, strlen("STAND METER :") * 5 + 4, 245, "RP " . rupiah($totalBayarPlusAdmin), $black);

$y = 265;
$x = 0;
foreach ($arrInfoTunggakan as $arr) {
    if ($x == 1) {
        $c = explode("\n", wordwrap($arr, 30, "\n"));
        foreach ($c as $cs) {
            imagestring($im, 1, (imagesx($im) - 5 * strlen($cs)) / 2, $y, $cs, $black);
            $y = $y + 10;
        }
    } else {
        imagestring($im, 1, (imagesx($im) - 5 * strlen($arr)) / 2, $y, $arr, $black);
    }
    $y = $y + 10;
    $x++;
}

imagestring($im, 1, 7, 325, $noid . '/' . $vsn . '/' . $penanda, $black);

