<?php

date_default_timezone_set("Asia/Jakarta");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//error_reporting(0);

require_once("Common/Func.php");
include_once '../mainconfig.php';

$id_trx = filter_input(INPUT_GET, 'id');
$url = MAIN_URL . 'framework/core/public/index.php/PRINTS/REQUEST/IMAGE/' . $id_trx;
$content = file_get_contents($url);
$obj = json_decode($content);

$createdat = date_format(date_create($obj->waktu), "Y-m-d H:i:s");

$footer = "RESI INI ADALAH";
$footer1 = "BUKTI PEMBAYARAN YANG SAH";
$im = imagecreatefrompng("images/struk1.png");
$black = imagecolorallocate($im, 0, 0, 0);
$grey = imagecolorallocate($im, 211, 211, 211);

imagestring($im, 1, (imagesx($im) - 4 * strlen($id_trx)) / 2, 85, $id_trx, $black);
imagestring($im, 1, (imagesx($im) - 5 * strlen($createdat)) / 2, 95, $createdat, $black);

if ($obj->product == "PULSA") {
	switch ($obj->product) {
		case 'PULSA' :  include 'Template/hp_pulsa.php';
			break;
		default: break;
	}
} else {
	switch ($obj->product . $obj->product_detail) {
		case 'PLNPREPAID' : include 'Template/pln_prepaid.php';
			break;
		case 'TELKOMHALO' : include 'Template/pasca_tsel.php';
			break;
		case 'TELKOMSPEEDY' : include 'Template/telkom_speedy.php';
			break;
		case 'TELKOMTELEPON' : include 'Template/pre_telkomsel.php';
			break;
		case 'BPJSKESEHATAN' : include 'Template/arsn_bpjs.php';
			break;
		case 'BPJSKETENAGAKERJAAN' : include 'Template/arsn_bpjs_ketenagakerjaan.php';
			break;
		case 'PLNNONTAGLIST' : include 'Template/pln_nontaglist.php';
			break;
		case 'PLNPOSTPAID' : include 'Template/pln_postpaid.php';
			break;
		case 'TELKOMVISION' : include 'Template/tv_transvision.php';
			break;
		case 'PDAMUNIVERSAL' : include 'Template/pdam_universal.php';
			break;
		case 'TIKETKAI' : include 'Template/tiket_kai.php';
			break;
		case 'TVAORA' : include 'Template/tv_aora.php';
			break;
		case 'TVINDOVISION' : include 'Template/tv_indovision.php';
			break;
		case 'TVBIG' : include 'Template/tv_big.php';
			break;
		default: break;
	}
}



imagestring($im, 1, (imagesx($im) + 20 ) / 3, imagesy($im) - 25, $footer, $black);
imagestring($im, 1, (imagesx($im) + 50 ) / 5, imagesy($im) - 15, $footer1, $black);

echo $id_struk = $id_trx;
$image = 'tmp_struk/' . $id_struk . '.png';
imagepng($im, $image);
imagedestroy($im);
$url = "http://akucek.com/igt/struk/".$image;
header("Location: $url");
//
//header("Content-type: image/png");
//readfile($image);
?>
