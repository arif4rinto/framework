<?php

header('Content-Type: application/json');
require_once 'config_url.php';

$param = array(
    'noid' => $_SESSION['noid'],
    'username' => USERNAME,
    'token' => $_SESSION['token'],
    'appid' => APPID
);

$json_request = json_encode($param);
$url_request = BASE_URL . 'REQUEST/act/PROCESS/menu_cek_saldo/WEB';
$response = getCurlResult($url_request, $json_request);
echo $response;
