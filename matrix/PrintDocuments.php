<?php 
session_start();

include 'WebClientPrint.php';
include_once '../mainconfig.php';

use Neodynamic\SDK\Web\WebClientPrint;    
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\ClientPrintJob;
$id_trx = filter_input(INPUT_GET, 'id');
$url = MAIN_URL.'framework/core/public/index.php/PRINTS/DOCUMENTS/DOTMATRIX/'.$id_trx;
$out = file_get_contents($url);
// $out = '0x1B@0x1B!0x01
// SEGI PEMBELIAN                                 STRUK PEMBELIAN LISTRIK PRABAYAR
// 0x0A NOMET :34008965005         NO METER  : 34008965005                MATERAI : Rp.          0,00
// 0x0 IDPEL :541300582512        IDPEL     : 541300582512               PPN     : Rp.          0,00
// 0x0 NAMA  :MUHAMAD SUDISMAN (B)NAMA      : MUHAMAD SUDISMAN (B)       PPJ     : Rp.      1.172,00
// 0x0 TARIF :R1/1300VA           TARIF/DAYA: R1/1300VA                  ANGSURAN: Rp.          0,00
// 0x0 STROOM:Rp.48.828,00        VSI REF   : 0BNS253515620404           RPSTROOM: Rp.     48.828,00
// 0x0 JMLKWH:36,2                            B79149109854407D           JML KWH :              36,2
// 0x0 ADMIN :Rp.1.800            RP BAYAR  : Rp. 51.800                 ADM BANK: Rp.         1.800
// 0x0 BAYAR :Rp.51.800           STROOM / TOKEN : 3737 9176 7346 8124 3816
// 0x0 VSIREF:0BNS253515620404
// 0x0        B79149109854407D    RINCIAN TAGIHAN DAPAT DIAKSES DI WWW.PLN.CO.ID ATAU PLN TERDEKAT
// 0x0A                           000300002535/20151008210942/FRF6I8B8/CU
// 0x0
// 0x0
// 0x0
// 0x0
// 0x0';
$urlParts = parse_url($_SERVER['REQUEST_URI']);

if (isset($urlParts['query'])){
    $rawQuery = $urlParts['query'];
    if($rawQuery[WebClientPrint::CLIENT_PRINT_JOB]){
        parse_str($rawQuery, $qs);
        $useDefaultPrinter = ($qs['useDefaultPrinter'] === 'checked');
        $printerName = urldecode($qs['printerName']);
        $cmds = $out;
        $cpj = new ClientPrintJob();
        $cpj->printerCommands = $cmds;
        $cpj->formatHexValues = true;
        if ($useDefaultPrinter || $printerName === 'null'){
            $cpj->clientPrinter = new DefaultPrinter();
        }else{
            $cpj->clientPrinter = new InstalledPrinter($printerName);
        }
        ob_clean();
        echo $cpj->sendToClient();
        exit();
    }
}
?>

    <script type="text/javascript">
        var wcppGetPrintersDelay_ms = 5000; //5 sec

        function wcpGetPrintersOnSuccess(){
            // Display client installed printers
            if(arguments[0].length > 0){
                var p=arguments[0].split("|");
                var options = '';
                for (var i = 0; i < p.length; i++) {
                    options += '<option>' + p[i] + '</option>';
                }
                $('#installedPrinters').css('visibility','visible');
                $('#installedPrinterName').html(options);
                $('#installedPrinterName').focus();
                $('#loadPrinters').hide();                                                        
            }else{
                alert("No printers are installed in your system.");
            }
        }

        function wcpGetPrintersOnFailure() {
            // Do something if printers cannot be got from the client
            alert("No printers are installed in your system.");
        }
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
       

